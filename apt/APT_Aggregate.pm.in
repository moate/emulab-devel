#!/usr/bin/perl -wT
#
# Copyright (c) 2007-2018 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
package APT_Aggregate;

use strict;
use English;
use Data::Dumper;
use Carp;
use Exporter;
use vars qw(@ISA @EXPORT $AUTOLOAD);

@ISA    = "Exporter";
@EXPORT = qw ( );

# Must come after package declaration!
use emdb;
use emutil;
use GeniHRN;
use overload ('""' => 'Stringify');

# Configure variables
my $TB		  = "@prefix@";
my $TBOPS         = "@TBOPSEMAIL@";
my $OURDOMAIN     = "@OURDOMAIN@";
my $MYURN	  = "urn:publicid:IDN+${OURDOMAIN}+authority+cm";

# Protos
sub STATUS($$;$);

#
# Lookup by uuid.
#
sub Lookup($$)
{
    my ($class, $token) = @_;
    my $safe_urn = DBQuoteSpecial($token);
    my $query_result;

    if (GeniHRN::IsValid($token)) {
	$query_result =
	    DBQueryWarn("select * from apt_aggregates where urn=$safe_urn");
    }
    else {
	return undef;
    }
    return undef
	if (!$query_result || !$query_result->numrows);

    my $self             = {};
    $self->{'AGGREGATE'} = $query_result->fetchrow_hashref();

    #
    # Look to see if there is a status row. Create it if it does not exist.
    #
    $query_result = 
	DBQueryWarn("select * from apt_aggregate_status where urn=$safe_urn");
    return undef
	if (!$query_result);

    if (!$query_result->numrows) {
	DBQueryWarn("replace into apt_aggregate_status set ".
		    " urn=$safe_urn, status='down'");
	$query_result = 
	    DBQueryWarn("select * from apt_aggregate_status ".
			"where urn=$safe_urn");
	return undef
	    if (!$query_result);
    }
    $self->{'STATUS'} = $query_result->fetchrow_hashref();

    bless($self, $class);
    return $self;
}

AUTOLOAD {
    my $self  = $_[0];
    my $type  = ref($self) or croak "$self is not an object";
    my $name  = $AUTOLOAD;
    $name =~ s/.*://;   # strip fully-qualified portion

    # A DB row proxy method call.
    if (exists($self->{'AGGREGATE'}->{$name})) {
	return $self->{'AGGREGATE'}->{$name};
    }
    elsif (exists($self->{'STATUS'}->{$name})) {
	#
	# We always want to go to the DB for this.
	#
	if (scalar(@_) == 2) {
	    return STATUS($self, $name, $_[1]);
	}
	else {
	    return STATUS($self, $name);
	}
    }
    carp("No such slot '$name' field in class $type");
    return undef;
}

# Break circular reference someplace to avoid exit errors.
sub DESTROY {
    my $self = shift;

    $self->{'AGGREGATE'} = undef;
}

sub IsLocalCluster($)
{
    my ($self) = @_;

    return ($self->urn() eq $MYURN ? 1 : 0);
}

#
# Refresh a class instance by reloading from the DB.
#
sub Refresh($)
{
    my ($self) = @_;

    return -1
	if (! ref($self));

    my $safe_urn = DBQuoteSpecial($self->urn());
    
    my $query_result =
	DBQueryWarn("select * from apt_aggregates where urn=$safe_urn");

    return -1
	if (!$query_result || !$query_result->numrows);

    $self->{'AGGREGATE'} = $query_result->fetchrow_hashref();

    #
    # Grab new status row.
    #
    $query_result = 
	DBQueryWarn("select * from apt_aggregate_status where urn=$safe_urn");
    return -1
	if (!$query_result);

    if ($query_result->numrows) {
	$self->{'STATUS'} = $query_result->fetchrow_hashref();
    }
    return 0;
}

#
# Stringify for output.
#
sub Stringify($)
{
    my ($self) = @_;
    my $urn    = $self->urn();

    return "[APT_Aggregate: $urn]";
}

#
# We always want to go to the DB when updating the status table.
#
sub STATUS($$;$)
{
    my ($self, $name, $newval) = @_;
    my $urn = $self->urn();

    if (!defined($newval)) {
	return $self->{'STATUS'}->{$name};
    }
    my $set = "";

    #
    # Convenience.
    #
    if (($name eq "last_success" || $name eq "last_attempt") &&
	$newval =~ /^\d+$/) {
	$newval = TBDateStringLocal($newval);
    }
    $set = "${name}=" . DBQuoteSpecial($newval);

    DBQueryWarn("update apt_aggregate_status set $set ".
		"where urn='$urn'")
	or return undef;

    $self->{'STATUS'}->{$name} = $newval;
    return $self->{'STATUS'}->{$name};
}

#
# Lookup all aggregates for a portal.
#
sub LookupForPortal($$)
{
    my ($class, $portal) = @_;
    my @result = ();

    if ($portal !~ /^[-\w]+$/) {
	return undef;
    }
    my $query_result =
	DBQueryWarn("select urn from apt_aggregates ".
		    "where disabled=0 and reservations=1 and ".
		    "      FIND_IN_SET('$portal', portals)");
    return ()
	if (!$query_result);
    return ()
	if (!$query_result->numrows);

    while (my ($aggregate_urn) = $query_result->fetchrow_array()) {
	my $aggregate = Lookup($class, $aggregate_urn);
	next
	    if (!defined($aggregate));
	push(@result, $aggregate);
    }
    return @result;
}

#
# Lookup using the short auth name (emulab.net).
#
sub LookupByDomain($$)
{
    my ($class, $domain) = @_;
    my @result = ();

    if ($domain !~ /^[-\w\.]+$/) {
	return undef;
    }
    my $query_result =
	DBQueryWarn("select urn from apt_aggregates ".
		    "where urn like 'urn:publicid:IDN+${domain}+%'");
    return undef
	if (!$query_result);
    return undef
	if (!$query_result->numrows);

    my ($aggregate_urn) = $query_result->fetchrow_array();
    return Lookup($class, $aggregate_urn);
}

sub LookupByNickname($$)
{
    my ($class, $nickname) = @_;

    if ($nickname !~ /^[-\w]+$/) {
	return undef;
    }
    my $query_result =
	DBQueryWarn("select urn from apt_aggregates ".
		    "where nickname='$nickname'");
    return undef
	if (!$query_result);
    return undef
	if (!$query_result->numrows);

    my ($aggregate_urn) = $query_result->fetchrow_array();
    return Lookup($class, $aggregate_urn);
}

#
# Check status of aggregate.
#
sub CheckStatus($$;$)
{
    my ($self, $perrmsg, $portalrpc) = @_;
    require APT_Geni;

    if ($self->disabled()) {
	$$perrmsg = "The " . $self->name() . " cluster ".
	    "is currently offline, please try again later.";
	return 1;
    }
    # Ping test. If we cannot get to the aggregate right now, bail.
    my $retval = APT_Geni::PingAggregate($self, $perrmsg, $portalrpc);
    if ($retval) {
	if ($retval < 0) {
	    $$perrmsg = "Internal error contacting the ".
		$self->name() . " cluster: " . $perrmsg;
	}
	else {
	    $$perrmsg = "The " . $self->name() . " cluster ".
		"is currently unreachable, please try again later.";
	}
	return 1;
    }
    return 0;
}

# _Always_ make sure that this 1 is at the end of the file...
1;
